package ru.sber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.model.human.employee.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Employee findByUsername(String username);
}
