package ru.sber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.model.bank.operator.Request;
import ru.sber.model.bank.operator.RequestCreateCardForm;

import java.util.List;

public interface RequestCreateCardFormRepository extends JpaRepository<RequestCreateCardForm, Long> {
    List<Request> findAllByClientId(Long id);
}
