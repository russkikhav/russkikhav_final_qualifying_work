package ru.sber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.model.bank.card.debit.DebitCard;

import java.util.List;

public interface DebitCardRepository extends JpaRepository<DebitCard, Long> {
    List<DebitCard> findAllByClientId(Long id);

    DebitCard findByCardNumber(String cardNumber);

    DebitCard findDebitCardByClientId(Long id);
}
