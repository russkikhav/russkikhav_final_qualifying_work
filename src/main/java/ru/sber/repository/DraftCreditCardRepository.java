package ru.sber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.model.bank.card.credit.DraftCreditCard;

public interface DraftCreditCardRepository extends JpaRepository<DraftCreditCard, Long> {
}
