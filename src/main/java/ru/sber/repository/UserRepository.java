package ru.sber.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.model.human.client.Client;

@Repository
public interface UserRepository extends JpaRepository<Client, Long> {
    Client findByUsername(String username);
}
