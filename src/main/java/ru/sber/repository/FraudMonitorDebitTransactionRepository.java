package ru.sber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.model.fraud.FraudMonitorDebitTransaction;

public interface FraudMonitorDebitTransactionRepository extends JpaRepository<FraudMonitorDebitTransaction, Long> {
}
