package ru.sber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.model.bank.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Account findClientByBankAccountNumber(String bankAccountNumber);
}
