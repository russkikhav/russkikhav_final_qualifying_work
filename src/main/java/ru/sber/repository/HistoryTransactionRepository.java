package ru.sber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.model.history.HistoryTransaction;

import java.util.List;

public interface HistoryTransactionRepository extends JpaRepository<HistoryTransaction, Long> {
    List<HistoryTransaction> findAllByDebitCardId(Long id);
}
