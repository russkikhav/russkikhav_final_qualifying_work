package ru.sber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.model.bank.card.credit.CreditCard;

import java.util.List;

public interface CreditCardRepository extends JpaRepository<CreditCard, Long> {
    List<CreditCard> findAllByClientId(Long id);

    CreditCard findByCardNumber(String cardNumber);
}
