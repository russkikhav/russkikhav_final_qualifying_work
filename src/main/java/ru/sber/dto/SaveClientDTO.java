package ru.sber.dto;

import lombok.Getter;
import lombok.Setter;
import ru.sber.model.bank.Account;
import ru.sber.model.bank.card.Card;
import ru.sber.model.human.Role;

import javax.validation.constraints.*;
import java.util.Set;

@Getter
@Setter
public class SaveClientDTO {

    //TODO err msg in prpties
    private static final String EMAIL_MESSAGE = "{email.message}";


    @NotEmpty(message = "field - \"Surname\" must not be empty")
    @Size(min = 2, max = 20, message = "\"Surname\" cannot be less than 2 and more than 20 characters")
    private String surname;

    @NotEmpty(message = "field - \"Name\" must not be empty")
    @Size(min = 2, max = 15, message = "\"Name\" cannot be less than 2 and more than 15 characters")
    private String name;

    @NotEmpty(message = "field - \"Second Name\" must not be empty")
    @Size(min = 5, max = 15, message = "\"Second Name\" cannot be less than 2 and more than 15 characters")
    private String secondName;

    @NotBlank
    private String dateOfBirth;

    @NotEmpty(message = "field - \"Passport Number\" must not be empty")
    @Pattern(regexp = "^(\\d{2}[- .]?){2}\\d{6}$", message = "the passport number must be of the standard \"33 33 333333\"")
    private String passportNumber;

    @NotEmpty(message = "field - \"Email\" must not be empty")
    @Email(message = SaveClientDTO.EMAIL_MESSAGE)
    private String email;

    @NotEmpty(message = "field - \"Username\" must not be empty")
    @Size(min = 8, max = 15, message = "\"Username\" cannot be less than 8 and more than 15 characters")
    private String username;

    @NotEmpty(message = "field - \"Password\" must not be empty")
    @Size(min = 8, max = 15, message = "\"Password\" cannot be less than 8 and more than 15 characters")
    private String password;

    @NotBlank
    private String confirmPassword;
    private Set<Card> cards;
    private Set<Role> roles;
    private Account accounts;
}
