package ru.sber.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter
@Setter
public class TransferFormDTO {
    @NotNull(message = "Введите сумму перевода")
    @Positive(message = "Значение должно быть больше нуля")
    @Min(value = 1, message = "нельзя перевести меньше 1 рубля")
    @Max(value = 100_000, message = "одновременный перевод не может быть больше 100 000 рублей")
    private Integer amount;

    @NotBlank
    private String cardOrBankAccountNumber;
    private String name;
}
