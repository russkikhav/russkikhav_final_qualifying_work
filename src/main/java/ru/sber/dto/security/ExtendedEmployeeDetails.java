package ru.sber.dto.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.sber.model.human.employee.Employee;

import java.util.Collection;

public class ExtendedEmployeeDetails implements UserDetails {
    private final Employee employee;

    public ExtendedEmployeeDetails(Employee employee) {
        this.employee = employee;
    }

    //TODO roles
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return employee.getRole();
    }

    @Override
    public String getPassword() {
        return employee.getPassword();
    }

    @Override
    public String getUsername() {
        return employee.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
