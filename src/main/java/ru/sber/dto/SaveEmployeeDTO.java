package ru.sber.dto;

import lombok.Getter;
import lombok.Setter;
import ru.sber.model.human.Role;
import ru.sber.model.human.employee.enumeration.EmployeePosition;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
public class SaveEmployeeDTO {
    private static final String NOT_EMPTY = "notEmpty.message";
    private static final String NOT_BLANK = "{notBlank.message}";
    private static final String SIZE = "{size.message}";

    @NotEmpty(message = SaveEmployeeDTO.NOT_EMPTY)
    @Size(min = 2, max = 20, message = SaveEmployeeDTO.SIZE)
    private String surname;

    @NotEmpty(message = SaveEmployeeDTO.NOT_EMPTY)
    @Size(min = 2, max = 20, message = SaveEmployeeDTO.SIZE)
    private String name;

    @NotEmpty(message = SaveEmployeeDTO.NOT_EMPTY)
    @Size(min = 2, max = 20, message = SaveEmployeeDTO.SIZE)
    private String secondName;

    private EmployeePosition position;

    @NotBlank(message = SaveEmployeeDTO.NOT_BLANK)
    private String username;

    @NotBlank(message = SaveEmployeeDTO.NOT_BLANK)
    private String password;

    private Set<Role> role;
}
