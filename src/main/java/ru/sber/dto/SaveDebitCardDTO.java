package ru.sber.dto;

import lombok.Getter;
import lombok.Setter;
import ru.sber.model.bank.card.debit.Bonus;
import ru.sber.model.bank.card.enumiration.PaymentSystem;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@Setter
public class SaveDebitCardDTO {
    @NotNull(message = "Установите начальный баланс")
    @Positive(message = "Значение должно быть больше нуля")
    @Min(value = 0, message = "нельзя установить баланс меньше, чем 0 рублей")
    @Max(value = 100_000, message = "изначальный баланс карты не может быть больше 100 000 рублей")
    private Double amount;

    private Bonus bonus;
    private PaymentSystem paymentSystem;
}
