package ru.sber.dto;

import lombok.Getter;
import lombok.Setter;
import ru.sber.model.bank.card.credit.Condition;
import ru.sber.model.bank.card.enumiration.PaymentSystem;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SaveCreditCardDTO {
    @NotNull
    private Double limitOnCredit;
    private Condition conditions;
    private PaymentSystem paymentSystem;

    @Override
    public String toString() {
        return "SaveCreditCardDTO{" +
                "limitOnCredit=" + limitOnCredit +
                ", conditions=" + conditions +
                ", paymentSystem=" + paymentSystem +
                '}';
    }
}
