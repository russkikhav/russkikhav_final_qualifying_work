package ru.sber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.sber.dto.SaveDebitCardDTO;
import ru.sber.exception.CardNotFoundException;
import ru.sber.model.bank.card.credit.enumeration.StatusCard;
import ru.sber.model.bank.card.debit.DebitCard;
import ru.sber.model.bank.card.enumiration.Bank;
import ru.sber.model.bank.card.enumiration.CardType;
import ru.sber.model.human.client.Client;
import ru.sber.repository.DebitCardRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class DebitCardService {
    private final DebitCardRepository debitCardRepository;
    private final ActualityAccountAmountService actualityAccountAmountService;

    @Autowired
    public DebitCardService(DebitCardRepository debitCardRepository, @Lazy ActualityAccountAmountService actualityAccountAmountService) {
        this.debitCardRepository = debitCardRepository;
        this.actualityAccountAmountService = actualityAccountAmountService;
    }

    public List<DebitCard> findAllByClientId(Long id) {
        return debitCardRepository.findAllByClientId(id);
    }

    public DebitCard findById(Long id) {
        Optional<DebitCard> value = debitCardRepository.findById(id);
        DebitCard debitCard = null;
        if (value.isPresent()) {
            debitCard = value.get();
        }
        if (debitCard == null) {
            throw new CardNotFoundException("Дебитовая карта " + id + " не найдена");
        }
        return debitCard;
    }

    public DebitCard findDebitCardByClientId(Long id) {
        return debitCardRepository.findDebitCardByClientId(id);
    }

    public void save(SaveDebitCardDTO draftDebitCardForm, Client client) {
        DebitCard draftDebitCard = new DebitCard();
        draftDebitCard.setDateOfRegistration(LocalDateTime.now());
        draftDebitCard.setClient(client);
        draftDebitCard.setCardType(CardType.DEBIT);
        draftDebitCard.setBank(Bank.SBER);
        draftDebitCard.setAmount(draftDebitCardForm.getAmount());
        draftDebitCard.setBonus(draftDebitCardForm.getBonus());
        draftDebitCard.setPaymentSystem(draftDebitCardForm.getPaymentSystem());
        draftDebitCard.setStatusCard(StatusCard.ACTIVE);

        actualityAccountAmountService.actualityAfterCreateDebitCard(draftDebitCard);
        debitCardRepository.save(draftDebitCard);
    }

}
