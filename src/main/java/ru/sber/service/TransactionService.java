package ru.sber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sber.dto.TransferFormDTO;
import ru.sber.exception.CardNotFoundException;
import ru.sber.exception.TransactionException;
import ru.sber.model.bank.card.debit.DebitCard;
import ru.sber.model.bank.card.enumiration.Bank;
import ru.sber.repository.DebitCardRepository;

import java.sql.SQLException;

@Service
public class TransactionService {
    private static final double COMMISSION_AFTER_TRANSACTION_IN_OTHER_BANK = 0.1;
    private static final int BANK_ACCOUNT_NUMBER_LENGTH = 12;

    private final DebitCardRepository debitCardRepository;
    private final HistoryService historyService;
    private final ActualityAccountAmountService actualityAccountAmountService;
    private final AccountService accountService;
    private final FraudMonitorDebitTransactionService fraudMonitorDebitTransactionService;

    @Autowired
    public TransactionService(DebitCardRepository debitCardRepository, HistoryService historyService,
                              ActualityAccountAmountService actualityAccountAmountService, AccountService accountService,
                              FraudMonitorDebitTransactionService fraudMonitorDebitTransactionService) {
        this.debitCardRepository = debitCardRepository;
        this.historyService = historyService;
        this.actualityAccountAmountService = actualityAccountAmountService;
        this.accountService = accountService;
        this.fraudMonitorDebitTransactionService = fraudMonitorDebitTransactionService;
    }

    @Transactional
    public void makeATransfer(DebitCard senderDebitCard, TransferFormDTO recipientTransferFormDTO) {
        if (recipientTransferFormDTO.getCardOrBankAccountNumber().length() > BANK_ACCOUNT_NUMBER_LENGTH) {
            doTransferOnCard(senderDebitCard, recipientTransferFormDTO);
        } else {
            doTransferOnAccount(senderDebitCard, recipientTransferFormDTO);
        }
    }

    @Transactional(rollbackFor = SQLException.class)
    public void doTransferOnCard(DebitCard senderDebitCard, TransferFormDTO recipientTransferFormDTO) {
        if (senderDebitCard.getAmount() >= recipientTransferFormDTO.getAmount() && recipientTransferFormDTO.getAmount() > 0) {
            if (senderDebitCard.getClient().getUsername().equals(recipientTransferFormDTO.getName())) {
                DebitCard recipientDebitCard = debitCardRepository.findByCardNumber(recipientTransferFormDTO.getCardOrBankAccountNumber());
                if (recipientDebitCard != null) {
                    if (recipientDebitCard.getBank() != Bank.SBER) {
                        makeACalcForClientAnotherBank(senderDebitCard, recipientTransferFormDTO, recipientDebitCard);
                    } else {
                        makeACalcForClientOurBank(senderDebitCard, recipientTransferFormDTO, recipientDebitCard);
                    }
                } else throw new TransactionException("Карты не существует");
            } else throw new TransactionException("Перевод не от своего имени");
        } else throw new TransactionException("Средств для перевода недостаточно");
    }

    @Transactional(rollbackFor = SQLException.class)
    public void doTransferOnAccount(DebitCard senderDebitCard, TransferFormDTO recipientTransferFormDTO) {
        if (senderDebitCard.getAmount() >= recipientTransferFormDTO.getAmount() && recipientTransferFormDTO.getAmount() > 0) {
            if (senderDebitCard.getClient().getUsername().equals(recipientTransferFormDTO.getName())) {
                DebitCard anyDebitCardRecipient = accountService.findAnyDebitCardByAccountNumber(recipientTransferFormDTO.getCardOrBankAccountNumber());
                if (anyDebitCardRecipient != null) {
                    makeACalcForClientOurBank(senderDebitCard, recipientTransferFormDTO, anyDebitCardRecipient);
                }
            } else throw new TransactionException("Перевод не от своего имени");
        } else throw new TransactionException("Средств для перевода недостаточно");
    }

    private void saveProgressAndSaveHistory(DebitCard senderDebitCard, TransferFormDTO recipientTransferFormDTO, DebitCard recipientDebitCard) {
        debitCardRepository.save(senderDebitCard);
        historyService.addToHistorySend(senderDebitCard, recipientTransferFormDTO);
        debitCardRepository.save(recipientDebitCard);
        historyService.addToHistoryGet(senderDebitCard, recipientTransferFormDTO, recipientDebitCard);
    }

    @Transactional(rollbackFor = SQLException.class)
    public void makeACalcForClientOurBank(DebitCard senderDebitCard, TransferFormDTO recipientTransferFormDTO, DebitCard recipientDebitCard) {
        System.out.println("recipientTransferFormDTO.getAmount() " + recipientTransferFormDTO.getAmount());
        if (recipientTransferFormDTO.getAmount() >= 50_000) {
            fraudMonitorDebitTransactionService.creteRequest(senderDebitCard, recipientTransferFormDTO, recipientDebitCard);
            senderDebitCard.setAmount(senderDebitCard.getAmount() - recipientTransferFormDTO.getAmount());
            actualityAccountAmountService.actualityAfterTransactionSender(senderDebitCard, recipientTransferFormDTO.getAmount());
            //throw new FraudMonitorTransactionException("Перевод будет осуществлен через Оператора банка");
        } else {
            senderDebitCard.setAmount(senderDebitCard.getAmount() - recipientTransferFormDTO.getAmount());
            recipientDebitCard.setAmount(recipientDebitCard.getAmount() + recipientTransferFormDTO.getAmount());
            actualityAccountAmountService.actualityAfterTransactionSender(senderDebitCard, recipientTransferFormDTO.getAmount());
            actualityAccountAmountService.actualityAfterTransactionRecipient(recipientDebitCard, recipientTransferFormDTO.getAmount());
        }
        saveProgressAndSaveHistory(senderDebitCard, recipientTransferFormDTO, recipientDebitCard);

    }

    //Комиссия в 10% пропадает
    @Transactional(rollbackFor = SQLException.class)
    public void makeACalcForClientAnotherBank(DebitCard senderDebitCard, TransferFormDTO recipientTransferFormDTO, DebitCard recipientDebitCard) {
        if (senderDebitCard.getAmount() >= (recipientDebitCard.getAmount() + (recipientTransferFormDTO.getAmount() * COMMISSION_AFTER_TRANSACTION_IN_OTHER_BANK))) {
            senderDebitCard.setAmount(senderDebitCard.getAmount() - (recipientDebitCard.getAmount() + (recipientTransferFormDTO.getAmount() * COMMISSION_AFTER_TRANSACTION_IN_OTHER_BANK)));
            recipientDebitCard.setAmount(recipientDebitCard.getAmount() + recipientTransferFormDTO.getAmount());
            actualityAccountAmountService.actualityAfterTransactionSender(senderDebitCard, recipientTransferFormDTO.getAmount());
            actualityAccountAmountService.actualityAfterTransactionRecipient(recipientDebitCard, recipientTransferFormDTO.getAmount());
        } else throw new TransactionException("Средств для перевода не достаточно");

        saveProgressAndSaveHistory(senderDebitCard, recipientTransferFormDTO, recipientDebitCard);
    }

    @Transactional(rollbackFor = SQLException.class)
    public void acceptedTransferAfterFraudMonitoring(Integer amount, String recipientDebitCardNumber) {
        DebitCard recipientDebitCard = debitCardRepository.findByCardNumber(recipientDebitCardNumber);
        recipientDebitCard.setAmount(recipientDebitCard.getAmount() + amount);
        debitCardRepository.save(recipientDebitCard);
        actualityAccountAmountService.actualityAfterTransactionRecipient(recipientDebitCard, amount);
    }

    @Transactional(rollbackFor = SQLException.class)
    public void deniedTransferAfterFraudMonitoring(DebitCard debitCardSender, Integer amountTransaction) {
        if (debitCardSender == null) {
            throw new CardNotFoundException("Карта пользователя не найдена");
        }
        debitCardSender.setAmount(debitCardSender.getAmount() + amountTransaction);
        debitCardRepository.save(debitCardSender);
        actualityAccountAmountService.actualityAfterTransactionSender(debitCardSender, amountTransaction);

    }
}
