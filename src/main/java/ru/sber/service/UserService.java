package ru.sber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.sber.dto.SaveClientDTO;
import ru.sber.dto.security.ExtendedClientDetails;
import ru.sber.model.bank.Account;
import ru.sber.model.bank.number.SetterNumberForAccount;
import ru.sber.model.human.Role;
import ru.sber.model.human.client.Client;
import ru.sber.repository.AccountRepository;
import ru.sber.repository.UserRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder cryptPasswordEncoder;
    private final AccountRepository accountRepository;

    @Autowired
    public UserService(UserRepository userRepository, BCryptPasswordEncoder cryptPasswordEncoder, AccountRepository accountRepository) {
        this.cryptPasswordEncoder = cryptPasswordEncoder;
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Client client = userRepository.findByUsername(username);
        if (client == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return new ExtendedClientDetails(client);
    }

    public boolean saveUser(SaveClientDTO clientDTO) {
        Client clientFromDB = userRepository.findByUsername(clientDTO.getUsername());

        if (clientFromDB != null) {
            return false;
        }

        clientFromDB = new Client();
        clientFromDB.setSurname(clientDTO.getSurname());
        clientFromDB.setName(clientDTO.getName());
        clientFromDB.setSecondName(clientDTO.getSecondName());
        clientFromDB.setDateOfBirth(clientDTO.getDateOfBirth());
        clientFromDB.setPassportNumber(clientDTO.getPassportNumber());
        clientFromDB.setEmail(clientDTO.getEmail());
        clientFromDB.setRoles(Collections.singleton(Role.ROLE_CLIENT));
        clientFromDB.setUsername(clientDTO.getUsername());
        clientFromDB.setPassword(cryptPasswordEncoder.encode(clientDTO.getPassword()));

        Account account = new Account();
        account.setBankAccountNumber(new SetterNumberForAccount().setNumberForAccount());
        account.setAmount(0d);
        clientFromDB.setAccounts(account);
        userRepository.save(clientFromDB);
        return true;
    }

    public Client findByUsername(String name) {
        return userRepository.findByUsername(name);
    }

    public Client findById(Long id) {
        Optional<Client> value = userRepository.findById(id);
        Client client = null;
        if (value.isPresent()) {
            client = value.get();
        } else throw new UsernameNotFoundException("Пользователя с таким Id не существует");

        return client;
    }

    public List<Client> findAll() {
        return userRepository.findAll();
    }

    public void save(Client editUser) {
        userRepository.save(editUser);
    }

    public void edit(Client client, Long id) {
        Client localClient = findById(id);
        localClient.setSurname(client.getSurname());
        localClient.setName(client.getName());
        localClient.setSecondName(client.getSecondName());
        localClient.setDateOfBirth(client.getDateOfBirth());
        localClient.setPassportNumber(client.getPassportNumber());
        localClient.setEmail(client.getEmail());
        localClient.setRoles(client.getRoles());

        userRepository.save(localClient);
    }
}
