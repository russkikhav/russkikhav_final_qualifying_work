package ru.sber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.sber.model.bank.Account;
import ru.sber.model.bank.card.debit.DebitCard;

import java.util.List;
import java.util.Objects;

@Service
public class ActualityAccountAmountService {

    private final UserService userService;
    private final AccountService accountService;
    private final DebitCardService debitCardService;

    @Autowired
    public ActualityAccountAmountService(UserService userService, AccountService accountService, @Lazy DebitCardService debitCardService) {
        this.userService = userService;
        this.accountService = accountService;
        this.debitCardService = debitCardService;
    }

    public void actualityAfterCreateDebitCard(DebitCard debitCard) {
        Account account = accountService.findById(debitCard.getClient().getId());
        account.setAmount(account.getAmount() + debitCard.getAmount());
    }

    public void actualityAfterTransactionSender(DebitCard senderDebitCard, Integer amount) {
        Long id = senderDebitCard.getClient().getId();

        Account account = accountService.findById(id);
        account.setAmount(account.getAmount() - amount);

        if (!account.getClient().getUsername().equals(senderDebitCard.getClient().getUsername())) {
            checkErrorTransaction(id);
        }
    }


    public void actualityAfterTransactionRecipient(DebitCard recipientDebitCard, Integer amount) {
        Long id = recipientDebitCard.getClient().getId();

        Account account = accountService.findById(id);
        account.setAmount(account.getAmount() + amount);

        if (!account.getClient().getUsername().equals(recipientDebitCard.getClient().getUsername())) {
            checkErrorTransaction(id);
        }

    }

    private void checkErrorTransaction(Long id) {
        List<DebitCard> debitCardSet = debitCardService.findAllByClientId(id);

        Double allMoneyDebitCard = debitCardSet
                .stream()
                .map(DebitCard::getAmount)
                .filter(Objects::nonNull)
                .mapToDouble(a -> a).sum();

        for (DebitCard d : debitCardSet) {
            System.out.println(d.getAmount());
        }

        Account account = accountService.findById(id);
        if (!Objects.equals(account.getAmount(), allMoneyDebitCard)) {
            throw new RuntimeException("СУММЫ не РАВНЫ " + allMoneyDebitCard + " != " + account.getAmount());
        }
    }
}
