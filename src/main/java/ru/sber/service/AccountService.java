package ru.sber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.exception.AccountNotFoundException;
import ru.sber.exception.CardNotFoundException;
import ru.sber.model.bank.Account;
import ru.sber.model.bank.card.debit.DebitCard;
import ru.sber.repository.AccountRepository;

import java.util.Optional;

@Service
public class AccountService {
    private final AccountRepository accountRepository;
    private final DebitCardService debitCardService;

    @Autowired
    public AccountService(AccountRepository accountRepository, DebitCardService debitCardService) {
        this.accountRepository = accountRepository;
        this.debitCardService = debitCardService;
    }

    public Account findById(Long id) {
        Optional<Account> value = accountRepository.findById(id);
        Account account = null;
        if (value.isPresent()) {
            account = value.get();
        }
        if (account == null) {
            throw new AccountNotFoundException("Аккаунт не найден");
        }
        return account;
    }

    public DebitCard findAnyDebitCardByAccountNumber(String cardOrBankAccountNumber) {
        Account account = accountRepository.findClientByBankAccountNumber(cardOrBankAccountNumber);
        Long id = account.getClient().getId();
        DebitCard anyDebitCardRecipient = debitCardService.findDebitCardByClientId(id);

        if (anyDebitCardRecipient == null) {
            throw new CardNotFoundException("У клиента " + cardOrBankAccountNumber + " нет ни одной дебитовой карты");
        }
        return anyDebitCardRecipient;
    }
}
