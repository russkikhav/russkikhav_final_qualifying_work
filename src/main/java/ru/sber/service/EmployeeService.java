package ru.sber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.sber.dto.SaveEmployeeDTO;
import ru.sber.dto.security.ExtendedEmployeeDetails;
import ru.sber.model.human.employee.Employee;
import ru.sber.repository.EmployeeRepository;

import java.util.List;

@Service
public class EmployeeService implements UserDetailsService {

    private final EmployeeRepository employeeRepository;
    private final BCryptPasswordEncoder cryptPasswordEncoder;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, BCryptPasswordEncoder cryptPasswordEncoder) {
        this.employeeRepository = employeeRepository;
        this.cryptPasswordEncoder = cryptPasswordEncoder;
    }

    public boolean saveEmployee(SaveEmployeeDTO saveEmployeeDTO) {
        Employee employeeFromDB = employeeRepository.findByUsername(saveEmployeeDTO.getUsername());

        if (employeeFromDB != null) {
            return false;
        }

        employeeFromDB = new Employee();
        employeeFromDB.setName(saveEmployeeDTO.getName());
        employeeFromDB.setSurname(saveEmployeeDTO.getSurname());
        employeeFromDB.setSecondName(saveEmployeeDTO.getSecondName());
        employeeFromDB.setPosition(saveEmployeeDTO.getPosition());
        employeeFromDB.setUsername(saveEmployeeDTO.getUsername());
        employeeFromDB.setPassword(cryptPasswordEncoder.encode(saveEmployeeDTO.getPassword()));
        employeeFromDB.setRole(saveEmployeeDTO.getRole());

        employeeRepository.save(employeeFromDB);

        return true;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Employee employee = employeeRepository.findByUsername(username);
        if (employee == null) {
            throw new UsernameNotFoundException("Такого сотрудника не существует");
        }
        return new ExtendedEmployeeDetails(employee);
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }
}
