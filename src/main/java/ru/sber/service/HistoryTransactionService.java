package ru.sber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.model.history.HistoryTransaction;
import ru.sber.repository.HistoryTransactionRepository;

import java.util.List;

@Service
public class HistoryTransactionService {
    private final HistoryTransactionRepository historyTransactionRepository;

    @Autowired
    public HistoryTransactionService(HistoryTransactionRepository historyTransactionRepository) {
        this.historyTransactionRepository = historyTransactionRepository;
    }

    public List<HistoryTransaction> findAllByDebitCardId(Long id) {
        return historyTransactionRepository.findAllByDebitCardId(id);
    }
}
