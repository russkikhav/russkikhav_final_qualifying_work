package ru.sber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.dto.TransferFormDTO;
import ru.sber.model.bank.card.debit.DebitCard;
import ru.sber.model.history.HistoryTransaction;
import ru.sber.model.history.emum.TransactionType;
import ru.sber.repository.HistoryTransactionRepository;

import javax.validation.Valid;
import java.time.LocalDateTime;

@Service
public class HistoryService {
    private final HistoryTransactionRepository historyTransactionRepository;

    @Autowired
    public HistoryService(HistoryTransactionRepository historyTransactionRepository) {
        this.historyTransactionRepository = historyTransactionRepository;
    }

    public void addToHistorySend(DebitCard senderDebitCard, @Valid TransferFormDTO transferCard) {
        HistoryTransaction historyTransaction = new HistoryTransaction();
        historyTransaction.setTransactionType(TransactionType.SEND);
        historyTransaction.setDebitCard(senderDebitCard);
        historyTransaction.setDateTime(LocalDateTime.now());
        historyTransaction.setAmount(transferCard.getAmount());
        historyTransaction.setSenderCardNumber(senderDebitCard.getCardNumber());
        historyTransaction.setRecipientCardNumber(transferCard.getCardOrBankAccountNumber());

        historyTransactionRepository.save(historyTransaction);
    }

    public void addToHistoryGet(DebitCard senderDebitCard, @Valid TransferFormDTO transferCard, DebitCard recipientDebitCard) {
        HistoryTransaction historyTransaction = new HistoryTransaction();
        historyTransaction.setTransactionType(TransactionType.GET);
        historyTransaction.setDebitCard(recipientDebitCard);
        historyTransaction.setDateTime(LocalDateTime.now());
        historyTransaction.setAmount(transferCard.getAmount());
        historyTransaction.setSenderCardNumber(senderDebitCard.getCardNumber());
        historyTransaction.setRecipientCardNumber(transferCard.getCardOrBankAccountNumber());

        historyTransactionRepository.save(historyTransaction);
    }
}
