package ru.sber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.exception.RequestNotFoundException;
import ru.sber.model.bank.operator.Request;
import ru.sber.model.bank.operator.RequestCreateCardForm;
import ru.sber.model.bank.operator.enumeration.RequestStatus;
import ru.sber.model.human.client.Client;
import ru.sber.repository.RequestCreateCardFormRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class RequestCreateCardFormService {
    private final RequestCreateCardFormRepository requestCreateCardFormRepository;

    @Autowired
    public RequestCreateCardFormService(RequestCreateCardFormRepository requestCreateCardFormRepository) {
        this.requestCreateCardFormRepository = requestCreateCardFormRepository;
    }

    public List<RequestCreateCardForm> findAll() {
        return requestCreateCardFormRepository.findAll();
    }

    public RequestCreateCardForm findById(Long id) {
        Optional<RequestCreateCardForm> value = requestCreateCardFormRepository.findById(id);
        RequestCreateCardForm requestCreateCardForm = null;
        if (value.isPresent()) {
            requestCreateCardForm = value.get();
        }
        if (requestCreateCardForm == null) {
            throw new RequestNotFoundException("Запрос " + id + " не найден");
        }

        return requestCreateCardForm;
    }

    public void save(RequestCreateCardForm requestCreateCardForm) {
        requestCreateCardFormRepository.save(requestCreateCardForm);
    }

    public List<Request> findAllByClientId(Long id) {
        return requestCreateCardFormRepository.findAllByClientId(id);
    }

    public void addNewRequest(RequestCreateCardForm requestCreateCardForm, Client client) {
        requestCreateCardForm.setId(null);
        requestCreateCardForm.setAmount(requestCreateCardForm.getAmount());
        requestCreateCardForm.setUsername(client.getUsername());
        requestCreateCardForm.setDateRequest(LocalDateTime.now());
        requestCreateCardForm.setRequestStatus(RequestStatus.IN_WORK);
        requestCreateCardForm.setClient(client);

        requestCreateCardFormRepository.save(requestCreateCardForm);
    }
}
