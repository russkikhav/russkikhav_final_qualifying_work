package ru.sber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.dto.TransferFormDTO;
import ru.sber.exception.FraudMonitorTransactionException;
import ru.sber.model.bank.card.debit.DebitCard;
import ru.sber.model.bank.operator.enumeration.RequestStatus;
import ru.sber.model.fraud.FraudMonitorDebitTransaction;
import ru.sber.repository.FraudMonitorDebitTransactionRepository;

import java.util.List;
import java.util.Optional;

@Service
public class FraudMonitorDebitTransactionService {
    private final FraudMonitorDebitTransactionRepository fraudMonitorDebitTransactionRepository;

    @Autowired
    public FraudMonitorDebitTransactionService(FraudMonitorDebitTransactionRepository fraudMonitorDebitTransactionRepository) {
        this.fraudMonitorDebitTransactionRepository = fraudMonitorDebitTransactionRepository;
    }

    public void creteRequest(DebitCard senderDebitCard, TransferFormDTO recipientTransferFormDTO, DebitCard recipientDebitCard) {

        FraudMonitorDebitTransaction fraudMonitorDebitTransaction = new FraudMonitorDebitTransaction();
        fraudMonitorDebitTransaction.setAmountTransaction(recipientTransferFormDTO.getAmount());
        fraudMonitorDebitTransaction.setDebitCardSender(senderDebitCard);
        fraudMonitorDebitTransaction.setRequestStatus(RequestStatus.IN_WORK);
        fraudMonitorDebitTransaction.setCardOrBankAccountNumberRecipient(recipientDebitCard.getCardNumber());

        fraudMonitorDebitTransactionRepository.save(fraudMonitorDebitTransaction);
    }

    public List<FraudMonitorDebitTransaction> findAll() {
        return fraudMonitorDebitTransactionRepository.findAll();
    }

    public FraudMonitorDebitTransaction findById(Long id) {
        Optional<FraudMonitorDebitTransaction> value = fraudMonitorDebitTransactionRepository.findById(id);
        FraudMonitorDebitTransaction fraudMonitorDebitTransaction = null;
        if (value.isPresent()) {
            fraudMonitorDebitTransaction = value.get();
        } else throw new FraudMonitorTransactionException("Запрос на проведение транзации оператором не найден");

        return fraudMonitorDebitTransaction;
    }

    public void save(FraudMonitorDebitTransaction fraudMonitorDebitTransaction) {
        fraudMonitorDebitTransactionRepository.save(fraudMonitorDebitTransaction);
    }
}
