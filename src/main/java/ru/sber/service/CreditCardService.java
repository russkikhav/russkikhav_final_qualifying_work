package ru.sber.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.dto.SaveCreditCardDTO;
import ru.sber.exception.CardNotFoundException;
import ru.sber.model.bank.card.credit.CreditCard;
import ru.sber.model.bank.card.credit.enumeration.StatusCard;
import ru.sber.model.bank.card.enumiration.Bank;
import ru.sber.model.bank.card.enumiration.CardType;
import ru.sber.model.human.client.Client;
import ru.sber.repository.CreditCardRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class CreditCardService {

    private final CreditCardRepository creditCardRepository;

    @Autowired
    public CreditCardService(CreditCardRepository creditCardRepository) {
        this.creditCardRepository = creditCardRepository;
    }

    public void save(SaveCreditCardDTO draftCreditCardForm, Client client) {

        CreditCard draftCreditCard = new CreditCard();
        draftCreditCard.setDateOfRegistration(LocalDateTime.now());
        draftCreditCard.setClient(client);
        draftCreditCard.setCardType(CardType.CREDIT);
        draftCreditCard.setBank(Bank.SBER);
        draftCreditCard.setLimitOnCredit(draftCreditCardForm.getLimitOnCredit());
        draftCreditCard.setConditions(draftCreditCardForm.getConditions());
        draftCreditCard.setPaymentSystem(draftCreditCardForm.getPaymentSystem());
        draftCreditCard.setStatusCard(StatusCard.DRAFT);

        creditCardRepository.save(draftCreditCard);
    }

    public void changeStatusForActive(CreditCard creditCard) {
        CreditCard draftCreditCard = creditCardRepository.findByCardNumber(creditCard.getCardNumber());
        if (draftCreditCard.getStatusCard() == StatusCard.DRAFT) {
            draftCreditCard.setStatusCard(StatusCard.ACTIVE);
        }

        creditCardRepository.save(draftCreditCard);
    }

    public void changeStatusForBlocked(CreditCard creditCard) {
        CreditCard draftCreditCard = creditCardRepository.findByCardNumber(creditCard.getCardNumber());
        if (draftCreditCard.getStatusCard() == StatusCard.DRAFT) {
            draftCreditCard.setStatusCard(StatusCard.BLOCK);
        }

        creditCardRepository.save(draftCreditCard);
    }

    public List<CreditCard> findAllByClientId(Long id) {
        return creditCardRepository.findAllByClientId(id);
    }

    public CreditCard findById(Long id) {
        Optional<CreditCard> value = creditCardRepository.findById(id);
        CreditCard creditCard = null;
        if (value.isPresent()) {
            creditCard = value.get();
        }
        if (creditCard == null) {
            throw new CardNotFoundException("Кредитная карта " + id + " не найдена");
        }
        return creditCard;
    }
}
