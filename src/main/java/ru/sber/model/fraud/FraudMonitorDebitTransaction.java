package ru.sber.model.fraud;

import lombok.Getter;
import lombok.Setter;
import ru.sber.model.bank.card.debit.DebitCard;
import ru.sber.model.bank.operator.enumeration.RequestStatus;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class FraudMonitorDebitTransaction {
    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    private DebitCard debitCardSender;

    private Integer amountTransaction;
    private String cardOrBankAccountNumberRecipient;

    @Enumerated(EnumType.STRING)
    private RequestStatus requestStatus;
    //todo request
}
