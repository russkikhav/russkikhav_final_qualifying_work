package ru.sber.model.human.employee;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.model.human.Role;
import ru.sber.model.human.employee.enumeration.EmployeePosition;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String surname;
    private String name;
    private String secondName;

    private EmployeePosition position;

    private String username;
    private String password;

    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "employee_role", joinColumns = @JoinColumn(name = "employee_id"))
    @Enumerated(EnumType.STRING)
    private Set<Role> role;

    public Employee(String surname, String name, String secondName, EmployeePosition position, String username, String password) {
        this.surname = surname;
        this.name = name;
        this.secondName = secondName;
        this.position = position;
        this.username = username;
        this.password = password;
    }

}
