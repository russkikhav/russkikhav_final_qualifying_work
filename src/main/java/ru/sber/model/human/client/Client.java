package ru.sber.model.human.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.model.bank.Account;
import ru.sber.model.bank.card.credit.CreditCard;
import ru.sber.model.bank.card.debit.DebitCard;
import ru.sber.model.human.Role;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@SuppressWarnings("serial")
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Client implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String surname;
    private String name;
    private String secondName;
    private String dateOfBirth;
    private String passportNumber;
    private String email;
    private String username;
    private String password;
    private boolean isActive;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "accounts_id")
    private Account accounts;

    @OneToMany
    private Set<DebitCard> debitCards;

    @OneToMany
    private Set<CreditCard> creditCards;

    @Transient
    private String confirmPassword;

    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "client_id"))
    @Enumerated(EnumType.STRING)
    private Set<Role> roles;

}
