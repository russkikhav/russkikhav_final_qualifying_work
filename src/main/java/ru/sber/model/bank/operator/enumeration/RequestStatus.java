package ru.sber.model.bank.operator.enumeration;

import lombok.Getter;

@Getter
public enum RequestStatus {
    IN_WORK("На рассмотрении"),
    DENIED("Отказано"),
    ACCEPTED("Одобрено");

    String status;

    RequestStatus(String status) {
        this.status = status;
    }
}
