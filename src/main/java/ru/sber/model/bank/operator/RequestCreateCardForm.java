package ru.sber.model.bank.operator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.model.bank.card.credit.CreditCard;
import ru.sber.model.bank.card.enumiration.CardType;
import ru.sber.model.bank.card.enumiration.PaymentSystem;
import ru.sber.model.bank.operator.enumeration.RequestStatus;
import ru.sber.model.human.client.Client;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class RequestCreateCardForm implements Request {

    @Id
    @GeneratedValue
    private Long id;

    @Transient
    private CreditCard creditCard;

    @ManyToOne
    private Client client;

    @Enumerated(EnumType.STRING)
    private PaymentSystem paymentSystem;

    @Enumerated(EnumType.STRING)
    private RequestStatus requestStatus;

    @Enumerated(EnumType.STRING)
    private CardType cardType;

    private Double amount;
    private String username;
    private LocalDateTime dateRequest;

    public RequestCreateCardForm(Double amountOfCredit, String username, LocalDateTime dateRequest) {
        this.amount = amountOfCredit;
        this.username = username;
        this.dateRequest = dateRequest;
    }
}
