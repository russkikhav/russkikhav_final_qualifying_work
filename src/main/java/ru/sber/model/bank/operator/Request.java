package ru.sber.model.bank.operator;

import ru.sber.model.bank.card.Card;
import ru.sber.model.bank.card.enumiration.CardType;

public interface Request {
    Long getId();

    CardType getCardType();

    Card getCreditCard();

    String getUsername();

    Double getAmount();

    String toString();
}
