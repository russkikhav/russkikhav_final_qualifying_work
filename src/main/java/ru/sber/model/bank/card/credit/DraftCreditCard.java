package ru.sber.model.bank.card.credit;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.model.bank.card.Card;
import ru.sber.model.bank.card.enumiration.Bank;
import ru.sber.model.bank.card.enumiration.CardType;
import ru.sber.model.bank.card.enumiration.PaymentSystem;
import ru.sber.model.bank.card.number.SetterNumberForCard;
import ru.sber.model.human.client.Client;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class DraftCreditCard implements Card {

    @Id
    @GeneratedValue
    private Long id;

    private String cardNumber;
    private Double limitOnCredit;
    private LocalDateTime dateOfCreateDraft;

    @Enumerated(EnumType.STRING)
    private Bank bank;

    @Enumerated(value = EnumType.STRING)
    @Convert(converter = ConditionConvertor.class)
    private Condition conditions;

    @ManyToOne
    private Client client;

    @Enumerated(EnumType.STRING)
    private PaymentSystem paymentSystem;

    @Enumerated(EnumType.STRING)
    private CardType cardType;

    @PrePersist
    private void setPrePersistDate() {
        setCardType(CardType.CREDIT);
        setCardNumber(new SetterNumberForCard().setNumberForCard(this));
        setDateOfCreateDraft(LocalDateTime.now());
    }

}
