package ru.sber.model.bank.card.enumiration;

public enum CardType {
    CREDIT,
    DEBIT
}
