package ru.sber.model.bank.card.debit;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.model.bank.card.Card;
import ru.sber.model.bank.card.credit.enumeration.StatusCard;
import ru.sber.model.bank.card.enumiration.Bank;
import ru.sber.model.bank.card.enumiration.CardType;
import ru.sber.model.bank.card.enumiration.PaymentSystem;
import ru.sber.model.bank.card.number.SetterNumberForCard;
import ru.sber.model.human.client.Client;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@SuppressWarnings("serial")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class DebitCard implements Card, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String cardNumber;
    private CardType cardType;
    private Double amount;
    private LocalDateTime dateOfRegistration;

    @Enumerated(EnumType.STRING)
    private StatusCard statusCard;

    @Enumerated(EnumType.STRING)
    private Bank bank;

    @Enumerated(EnumType.STRING)
    private Bonus bonus;

    @Enumerated(EnumType.STRING)
    private PaymentSystem paymentSystem;

    @ManyToOne
    private Client client;

    @PrePersist
    private void setCardNumber() {
        setCardType(CardType.DEBIT);
        setCardNumber(new SetterNumberForCard().setNumberForCard(this));
    }

}