package ru.sber.model.bank.card.enumiration;

import lombok.Getter;

@Getter
public enum PaymentSystem {
    VISA,
    MASTER,
    MIR
}
