package ru.sber.model.bank.card.number;

import ru.sber.model.bank.card.Card;
import ru.sber.model.bank.card.enumiration.CardType;

import java.util.Random;

public class SetterNumberForCard {
    private static final String CREDIT_MIR_CARD_PREFIX = "220163";
    private static final String CREDIT_VISA_CARD_PREFIX = "450163";
    private static final String CREDIT_MASTER_CARD_PREFIX = "500163";

    private static final String DEBIT_MIR_CARD_PREFIX = "220263";
    private static final String DEBIT_VISA_CARD_PREFIX = "450263";
    private static final String DEBIT_MASTER_CARD_PREFIX = "500263";

    private final Random random = new Random();

    public String setNumberForCard(Card card) {
        String cardNumber = "";
        CardType cardType = card.getCardType();
        if (cardType == CardType.CREDIT) {
            switch (card.getPaymentSystem()) {
                case MIR:
                    cardNumber += CREDIT_MIR_CARD_PREFIX + getIdentificationNumberCard() + getCheckNumber();
                    break;
                case VISA:
                    cardNumber += CREDIT_VISA_CARD_PREFIX + getIdentificationNumberCard() + getCheckNumber();
                    break;
                case MASTER:
                    cardNumber += CREDIT_MASTER_CARD_PREFIX + getIdentificationNumberCard() + "" + getCheckNumber();
                    break;
            }
        } else if (cardType == CardType.DEBIT) {
            switch (card.getPaymentSystem()) {
                case MIR:
                    cardNumber += DEBIT_MIR_CARD_PREFIX + getIdentificationNumberCard() + getCheckNumber();
                    break;
                case VISA:
                    cardNumber += DEBIT_VISA_CARD_PREFIX + getIdentificationNumberCard() + getCheckNumber();
                    break;
                case MASTER:
                    cardNumber += DEBIT_MASTER_CARD_PREFIX + getIdentificationNumberCard() + getCheckNumber();
                    break;
            }
        }
        return cardNumber;
    }

    private String getIdentificationNumberCard() {

        StringBuilder identificationNumber = new StringBuilder();
        for (int i = 0; i < 9; i++) {
            int randNumber = random.nextInt(9);
            identificationNumber.append(randNumber);
        }
        return identificationNumber.toString();
    }

    private String getCheckNumber() {
        int randNumber = random.nextInt(9);
        return Integer.toString(randNumber);
    }
}
