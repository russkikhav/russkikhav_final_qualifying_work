package ru.sber.model.bank.card.credit;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.model.bank.card.Card;
import ru.sber.model.bank.card.credit.enumeration.StatusCard;
import ru.sber.model.bank.card.enumiration.Bank;
import ru.sber.model.bank.card.enumiration.CardType;
import ru.sber.model.bank.card.enumiration.PaymentSystem;
import ru.sber.model.bank.card.number.SetterNumberForCard;
import ru.sber.model.human.client.Client;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@SuppressWarnings("serial")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class CreditCard implements Card, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String cardNumber;
    private Double limitOnCredit;
    private LocalDateTime dateOfRegistration;

    @Enumerated(EnumType.STRING)
    private Bank bank;

    @Enumerated(EnumType.STRING)
    private StatusCard statusCard;

    @Enumerated(value = EnumType.STRING)
    @Convert(converter = ConditionConvertor.class)
    private Condition conditions;

    @ManyToOne
    private Client client;

    @Enumerated(EnumType.STRING)
    private PaymentSystem paymentSystem;

    @Enumerated(EnumType.STRING)
    private CardType cardType;

    public CreditCard(String cardNumber, Double limitOnCredit, Condition conditions, PaymentSystem cardType) {
        this.cardNumber = cardNumber;
        this.limitOnCredit = limitOnCredit;
        this.conditions = conditions;
        this.paymentSystem = cardType;
    }

    @PrePersist
    private void setPrePersistDate() {
        setCardType(CardType.CREDIT);
        setCardNumber(new SetterNumberForCard().setNumberForCard(this));
        setDateOfRegistration(LocalDateTime.now());
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "id=" + id +
                ", cardNumber='" + cardNumber + '\'' +
                ", limitOnCredit=" + limitOnCredit +
                ", conditions=" + conditions +
                ", client=" + client +
                ", cardType=" + paymentSystem +
                '}';
    }
}
