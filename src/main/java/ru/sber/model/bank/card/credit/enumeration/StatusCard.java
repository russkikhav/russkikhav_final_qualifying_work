package ru.sber.model.bank.card.credit.enumeration;

public enum StatusCard {
    DRAFT,
    ACTIVE,
    BLOCK
}
