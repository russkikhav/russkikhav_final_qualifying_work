package ru.sber.model.bank.card.credit;

import lombok.Getter;

@Getter
public enum Condition {
    LOYAL_CONDITION(5.0),
    MEDIUM_CONDITION(10.0),
    HARD_CONDITION(15.0),
    ;

    private final Double percentCredit;

    Condition(Double percentCredit) {
        this.percentCredit = percentCredit;
    }
}
