package ru.sber.model.bank.card.credit;

import ru.sber.exception.ConvertorException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Objects;

@Converter
public class ConditionConvertor implements AttributeConverter<Condition, Double> {
    @Override
    public Double convertToDatabaseColumn(Condition condition) throws ConvertorException {
        switch (condition) {
            case HARD_CONDITION:
                return Condition.HARD_CONDITION.getPercentCredit();
            case MEDIUM_CONDITION:
                return Condition.MEDIUM_CONDITION.getPercentCredit();
            case LOYAL_CONDITION:
                return Condition.LOYAL_CONDITION.getPercentCredit();
            default:
                throw new ConvertorException("Условиe " + condition + " не поддерживается");
        }
    }

    @Override
    public Condition convertToEntityAttribute(Double aDouble) {
        if (Objects.equals(aDouble, Condition.HARD_CONDITION.getPercentCredit())) {
            return Condition.HARD_CONDITION;
        } else if (Objects.equals(aDouble, Condition.MEDIUM_CONDITION.getPercentCredit())) {
            return Condition.MEDIUM_CONDITION;
        } else if (Objects.equals(aDouble, Condition.LOYAL_CONDITION.getPercentCredit())) {
            return Condition.LOYAL_CONDITION;
        }
        throw new ConvertorException("Процента: " + aDouble + " не существует в условиях банка");
    }
}
