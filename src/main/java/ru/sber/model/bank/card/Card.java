package ru.sber.model.bank.card;


import ru.sber.model.bank.card.enumiration.CardType;
import ru.sber.model.bank.card.enumiration.PaymentSystem;

public interface Card {
    CardType getCardType();

    PaymentSystem getPaymentSystem();
}
