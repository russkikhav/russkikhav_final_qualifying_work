package ru.sber.model.bank.card.debit;

import lombok.Getter;

@Getter
public enum Bonus {
    SBER_SPASIBO(0) {
        @Override
        public void setBonus(Integer bonus) {
            bonusPoint += bonus;
        }
    },
    OTHER_BONUS(0) {
        @Override
        public void setBonus(Integer bonus) {
            bonusPoint += bonus;
        }
    };


    Integer bonusPoint;

    Bonus(Integer bonusPoint) {
        this.bonusPoint = bonusPoint;
    }

    abstract void setBonus(Integer bonus);
}