package ru.sber.model.bank.number;

import java.util.Random;

public class SetterNumberForAccount {
    private static final String BANK_PREFIX = "10";

    private final Random random = new Random();

    public String setNumberForAccount() {
        StringBuilder accountNumber = new StringBuilder(BANK_PREFIX);
        for (int i = 0; i < 10; i++) {
            int randomNumber = random.nextInt(9);
            accountNumber.append(randomNumber);
        }
        return accountNumber.toString();
    }
}
