package ru.sber.model.bank;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.model.human.client.Client;

import javax.persistence.*;
import java.io.Serializable;

@SuppressWarnings("serial")
@Entity
@Setter
@Getter
@NoArgsConstructor
public class Account implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String bankAccountNumber;
    private Double amount;

    @OneToOne(mappedBy = "accounts")
    private Client client;

    public Account(String bankAccountNumber, Double amount) {
        this.bankAccountNumber = bankAccountNumber;
        this.amount = amount;
    }
}
