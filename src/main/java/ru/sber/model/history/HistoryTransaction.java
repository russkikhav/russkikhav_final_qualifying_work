package ru.sber.model.history;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.model.bank.card.debit.DebitCard;
import ru.sber.model.history.emum.TransactionType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class HistoryTransaction {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private DebitCard debitCard;

    private LocalDateTime dateTime;
    private String recipientCardNumber;
    private String senderCardNumber;
    private Integer amount;
    private TransactionType transactionType;
}
