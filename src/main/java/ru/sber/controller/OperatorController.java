package ru.sber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.sber.dto.SaveCreditCardDTO;
import ru.sber.dto.SaveDebitCardDTO;
import ru.sber.model.bank.card.credit.Condition;
import ru.sber.model.bank.card.credit.CreditCard;
import ru.sber.model.bank.card.debit.Bonus;
import ru.sber.model.bank.card.debit.DebitCard;
import ru.sber.model.bank.card.enumiration.CardType;
import ru.sber.model.bank.card.enumiration.PaymentSystem;
import ru.sber.model.bank.operator.Request;
import ru.sber.model.bank.operator.RequestCreateCardForm;
import ru.sber.model.bank.operator.enumeration.RequestStatus;
import ru.sber.model.fraud.FraudMonitorDebitTransaction;
import ru.sber.service.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/operator")
@PreAuthorize("hasAuthority('ROLE_EMPLOYEE')")
public class OperatorController {
    private static final String DRAFT_CREDIT_CARD_REQUEST_VIEW = "/operator/card/credit/request_edit";
    private static final String DRAFT_DEBIT_CARD_REQUEST_VIEW = "/operator/card/debit/request_edit";
    private static final String ALL_FRAUD_REQUEST_VIEW = "/operator/fraud/request";
    private static final String FRAUD_REQUEST_VIEW = "/operator/fraud/request_edit";
    private static final String REDIRECT_OPERATOR = "redirect:/operator";

    private final RequestCreateCardFormService requestCreateCardFormService;
    private final UserService userService;
    private final CreditCardService creditCardService;
    private final DebitCardService debitCardService;
    private final FraudMonitorDebitTransactionService fraudMonitorDebitTransactionService;
    private final TransactionService transactionService;

    @Autowired
    public OperatorController(RequestCreateCardFormService requestCreateCardFormService,
                              UserService userService, CreditCardService creditCardService, DebitCardService debitCardService, FraudMonitorDebitTransactionService fraudMonitorDebitTransactionService, TransactionService transactionService) {
        this.requestCreateCardFormService = requestCreateCardFormService;
        this.userService = userService;
        this.creditCardService = creditCardService;
        this.debitCardService = debitCardService;
        this.fraudMonitorDebitTransactionService = fraudMonitorDebitTransactionService;
        this.transactionService = transactionService;
    }

    @GetMapping
    public String request(Model model) {
        model.addAttribute("request_credit_cards", requestCreateCardFormService.findAll());
        return "/operator/card/request";
    }

    @GetMapping("/done")
    public String doneRequest(Model model) {
        model.addAttribute("request_credit_cards", requestCreateCardFormService.findAll());
        return "/operator/card/done_request";
    }

    @GetMapping("/request/{id}")
    public String checkRequest(@PathVariable("id") Long id, Model model) {
        Request request = requestCreateCardFormService.findById(id);
        model.addAttribute("credit_card", request.getCreditCard());
        model.addAttribute("request_credit_card", request);
        model.addAttribute("conditions", Condition.values());
        model.addAttribute("payment_system", PaymentSystem.values());
        model.addAttribute("client", userService.findByUsername(request.getUsername()));
        model.addAttribute("credit_card_form", new CreditCard());
        model.addAttribute("debit_card_form", new DebitCard());
        model.addAttribute("bonuses", Bonus.values());

        return request.getCardType() == CardType.CREDIT ? DRAFT_CREDIT_CARD_REQUEST_VIEW : DRAFT_DEBIT_CARD_REQUEST_VIEW;
    }

    @PostMapping("/request/credit/{id}")
    public String createCreditCardForClient(@PathVariable("id") Long id,
                                            @ModelAttribute("draft_credit_card") @Valid SaveCreditCardDTO draftCreditCardForm,
                                            BindingResult bindingResult,
                                            @RequestParam(required = true, defaultValue = "") String username) {

        if (bindingResult.hasErrors()) {
            return DRAFT_CREDIT_CARD_REQUEST_VIEW;
        }
        RequestCreateCardForm requestCreateCardForm = requestCreateCardFormService.findById(id);
        requestCreateCardForm.setRequestStatus(RequestStatus.ACCEPTED);
        requestCreateCardFormService.save(requestCreateCardForm);
        creditCardService.save(draftCreditCardForm, userService.findByUsername(username));

        return REDIRECT_OPERATOR;
    }

    @PostMapping("/request/debit/{id}")
    public String createDebitCardForClient(@PathVariable("id") Long id,
                                           @ModelAttribute("draft_debit_card") @Valid SaveDebitCardDTO draftDebitCardForm,
                                           BindingResult bindingResult,
                                           @RequestParam(required = true, defaultValue = "") String username) {

        if (bindingResult.hasErrors()) {
            return DRAFT_DEBIT_CARD_REQUEST_VIEW;
        }
        RequestCreateCardForm requestCreateCardForm = requestCreateCardFormService.findById(id);
        requestCreateCardForm.setRequestStatus(RequestStatus.ACCEPTED);
        requestCreateCardFormService.save(requestCreateCardForm);
        debitCardService.save(draftDebitCardForm, userService.findByUsername(username));

        return REDIRECT_OPERATOR;
    }

    @PostMapping("/request/denied/{id}")
    public String answerForClient(@PathVariable("id") Long id) {
        RequestCreateCardForm requestCreateCardForm = requestCreateCardFormService.findById(id);
        requestCreateCardForm.setRequestStatus(RequestStatus.DENIED);
        requestCreateCardFormService.save(requestCreateCardForm);

        return REDIRECT_OPERATOR;
    }

    @GetMapping("/request/fraud")
    public String allFraudMonitoringRequest(Model model) {
        model.addAttribute("fraud_request", fraudMonitorDebitTransactionService.findAll());

        return ALL_FRAUD_REQUEST_VIEW;
    }

    @GetMapping("/request/fraud/{id}")
    public String onceFraudMonitoringRequest(@PathVariable("id") Long id, Model model) {
        FraudMonitorDebitTransaction fraudMonitorDebitTransaction = fraudMonitorDebitTransactionService.findById(id);
        model.addAttribute("fraud_request", fraudMonitorDebitTransaction);
        return FRAUD_REQUEST_VIEW;
    }

    @PostMapping("/request/fraud/accept/{id}")
    public String acceptFraudTransaction(@PathVariable("id") Long id) {
        FraudMonitorDebitTransaction fraudMonitorDebitTransaction = fraudMonitorDebitTransactionService.findById(id);
        fraudMonitorDebitTransaction.setRequestStatus(RequestStatus.ACCEPTED);
        transactionService.acceptedTransferAfterFraudMonitoring(fraudMonitorDebitTransaction.getAmountTransaction(),
                fraudMonitorDebitTransaction.getCardOrBankAccountNumberRecipient());

        fraudMonitorDebitTransactionService.save(fraudMonitorDebitTransaction);
        return REDIRECT_OPERATOR;
    }

    @PostMapping("/request/fraud/denied/{id}")
    public String deniedFraudTransaction(@PathVariable("id") Long id) {
        FraudMonitorDebitTransaction fraudMonitorDebitTransaction = fraudMonitorDebitTransactionService.findById(id);
        fraudMonitorDebitTransaction.setRequestStatus(RequestStatus.DENIED);
        transactionService.deniedTransferAfterFraudMonitoring(fraudMonitorDebitTransaction.getDebitCardSender(),
                fraudMonitorDebitTransaction.getAmountTransaction());

        fraudMonitorDebitTransactionService.save(fraudMonitorDebitTransaction);
        return REDIRECT_OPERATOR;
    }
}
