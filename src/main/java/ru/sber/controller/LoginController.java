package ru.sber.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {
    private static final String LOGIN_VIEW = "/login";

    @GetMapping
    public String login() {
        return LOGIN_VIEW;
    }
}
