package ru.sber.controller;

import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sber.model.bank.card.credit.CreditCard;
import ru.sber.model.bank.card.credit.enumeration.StatusCard;
import ru.sber.model.bank.card.debit.DebitCard;
import ru.sber.model.human.client.Client;
import ru.sber.service.*;

import java.security.Principal;

@Controller
@RequestMapping("/bank")
@PreAuthorize("hasAnyAuthority('ROLE_CLIENT','ROLE_EMPLOYEE')")
public class BankController {
    private static final String REDIRECT_HOME = "redirect:/";

    private final DebitCardService debitCardService;
    private final CreditCardService creditCardService;
    private final UserService userService;
    private final RequestCreateCardFormService requestCreateCardFormService;
    private final AccountService accountService;

    public BankController(@Lazy DebitCardService debitCardService, CreditCardService creditCardService,
                          UserService userService, RequestCreateCardFormService requestCreateCardFormService, AccountService accountService) {
        this.debitCardService = debitCardService;
        this.creditCardService = creditCardService;
        this.userService = userService;
        this.requestCreateCardFormService = requestCreateCardFormService;
        this.accountService = accountService;
    }

    @ModelAttribute("module")
    public String module() {
        return "banks";
    }

    @GetMapping
    public String bank(Model model, Principal principal) {
        Client client = userService.findByUsername(principal.getName());
        model.addAttribute("client", client);
        model.addAttribute("account", accountService.findById(client.getId()));
        return "bank/bankHome";
    }

    @GetMapping("/{id}")
    public String bank(@PathVariable("id") Client client, Model model) {

        model.addAttribute("client_request", requestCreateCardFormService.findAllByClientId(client.getId()));
        model.addAttribute("client", client);
        model.addAttribute("credit_cards", creditCardService.findAllByClientId(client.getId()));
        model.addAttribute("debit_cards", debitCardService.findAllByClientId(client.getId()));
        return "bank/bank";
    }

    @GetMapping("/credit_card/{id}")
    public String creditCardInfo(@PathVariable("id") Long id, Model model) {
        CreditCard creditCard = creditCardService.findById(id);
        model.addAttribute("credit_card", creditCard);
        model.addAttribute("card_status", StatusCard.DRAFT);
        return "bank/credit_card_info";
    }

    @GetMapping("/debit_card/{id}")
    public String debitCardInfo(@PathVariable("id") Long id, Model model) {
        DebitCard debitCard = debitCardService.findById(id);
        model.addAttribute("debit_card", debitCard);
        return "bank/debit_card_info";
    }

    @PostMapping("credit/change/accept/{id}")
    public String changeStatusAccept(@PathVariable("id") CreditCard draftCreditCard) {
        creditCardService.changeStatusForActive(draftCreditCard);
        return REDIRECT_HOME;
    }

    @PostMapping("credit/change/denied/{id}")
    public String changeStatusDenied(@PathVariable("id") CreditCard draftCreditCard) {
        creditCardService.changeStatusForBlocked(draftCreditCard);
        return REDIRECT_HOME;
    }
}
