package ru.sber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sber.service.HistoryTransactionService;

@Controller
@RequestMapping("/history")
public class HistoryController {
    private static final String HISTORY_VIEW = "bank/history/history";

    private final HistoryTransactionService historyTransactionService;

    @Autowired
    public HistoryController(HistoryTransactionService historyTransactionService) {

        this.historyTransactionService = historyTransactionService;
    }

    @GetMapping("/{id}")
    public String history(@PathVariable Long id, Model model) {

        model.addAttribute("histories", historyTransactionService.findAllByDebitCardId(id));

        return HISTORY_VIEW;
    }
}
