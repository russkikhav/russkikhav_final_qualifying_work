package ru.sber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sber.dto.SaveEmployeeDTO;
import ru.sber.model.human.Role;
import ru.sber.model.human.employee.enumeration.EmployeePosition;
import ru.sber.service.EmployeeService;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
public class AdminController {
    private static final String CREATE_NEW_EMPLOYEE = "user/admin/admin";

    private final EmployeeService employeeService;

    @Autowired
    public AdminController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @ModelAttribute("module")
    public String module() {
        return "admin";
    }

    @GetMapping
    public String admin(@ModelAttribute("saveEmployee") SaveEmployeeDTO saveEmployee, Model model) {
        model.addAttribute("positions", EmployeePosition.values());
        model.addAttribute("roles", Role.values());
        return CREATE_NEW_EMPLOYEE;
    }

    @PostMapping
    public String saveNewEmployee(@ModelAttribute("saveEmployee") @Valid SaveEmployeeDTO saveEmployee, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return CREATE_NEW_EMPLOYEE;
        }
        if (!employeeService.saveEmployee(saveEmployee)) {
            model.addAttribute("usernameError", "Пользователь с таким логином уже существует");
            return CREATE_NEW_EMPLOYEE;
        }
        return "redirect:/";
    }
}
