package ru.sber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.sber.model.bank.card.enumiration.CardType;
import ru.sber.model.bank.card.enumiration.PaymentSystem;
import ru.sber.model.bank.operator.RequestCreateCardForm;
import ru.sber.service.RequestCreateCardFormService;
import ru.sber.service.UserService;

import javax.validation.Valid;


@Controller
@RequestMapping("/card")
@PreAuthorize("hasAnyAuthority('ROLE_CLIENT')")
public class CardController {
    private static final String REDIRECT_BANK_ID = "redirect:/bank/{id}";
    private static final String REQUEST_CREDIT_CARD = "card/credit_card_request";
    private static final String REQUEST_DEBIT_CARD = "card/debit_card_request";

    private final RequestCreateCardFormService requestCreateCardFormService;
    private final UserService userService;

    @Autowired
    public CardController(RequestCreateCardFormService requestCreateCardFormService, UserService userService) {
        this.requestCreateCardFormService = requestCreateCardFormService;
        this.userService = userService;
    }

    @GetMapping("request_credit_card/{id}")
    public String requestForCreateCreditCard(@PathVariable("id") Long id, Model model) {
        model.addAttribute("client", userService.findById(id));
        model.addAttribute("request_credit_card", new RequestCreateCardForm());

        return REQUEST_CREDIT_CARD;
    }

    @PostMapping("request_credit_card/{id}")
    public String addRequestForCreateCreditCard(@PathVariable Long id,
                                                @ModelAttribute("request_credit_card") @Valid RequestCreateCardForm requestCreateCardForm,
                                                BindingResult bindingResult, Model model,
                                                @RequestParam(required = true, defaultValue = "") String username) {

        if (bindingResult.hasErrors()) {
            return REQUEST_CREDIT_CARD;
        }

        requestCreateCardForm.setCardType(CardType.CREDIT);
        requestCreateCardFormService.addNewRequest(requestCreateCardForm, userService.findByUsername(username));

        return REDIRECT_BANK_ID;
    }

    @GetMapping("request_debit_card/{id}")
    public String requestForCreateDebitCard(@PathVariable("id") Long id, Model model) {

        model.addAttribute("client", userService.findById(id));
        model.addAttribute("request_debit_card", new RequestCreateCardForm());
        model.addAttribute("payment_system", PaymentSystem.values());

        return REQUEST_DEBIT_CARD;
    }

    @PostMapping("request_debit_card/{id}")
    public String addRequestForCreateDebitCard(@PathVariable Long id,
                                               @ModelAttribute("request_debit_card") @Valid RequestCreateCardForm requestCreateCardForm,
                                               BindingResult bindingResult,
                                               @RequestParam(required = true, defaultValue = "") String username) {

        if (bindingResult.hasErrors()) {
            return REQUEST_DEBIT_CARD;
        }

        requestCreateCardForm.setCardType(CardType.DEBIT);
        requestCreateCardFormService.addNewRequest(requestCreateCardForm, userService.findByUsername(username));

        return REDIRECT_BANK_ID;
    }

}
