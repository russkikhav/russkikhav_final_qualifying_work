package ru.sber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sber.service.EmployeeService;

@Controller
@RequestMapping("/employers")
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
public class EmployeeController {
    private static final String EMPLOYEES_VIEW = "/user/employers";

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public String employee(Model model) {
        model.addAttribute("employers", employeeService.findAll());
        return EMPLOYEES_VIEW;
    }
}
