package ru.sber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sber.dto.SaveClientDTO;
import ru.sber.service.UserService;

import javax.validation.Valid;

@Controller
@RequestMapping("/registration")
public class RegistrationController {
    private static final String REGISTRATION_VIEW = "registration";
    private static final String REDIRECT_LOGIN_VIEW = "redirect:/login";

    private final UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String registration(@ModelAttribute("userForm") SaveClientDTO userForm) {
        return REGISTRATION_VIEW;
    }

    @PostMapping
    public String addNewUser(@ModelAttribute("userForm") @Valid SaveClientDTO userForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return REGISTRATION_VIEW;
        }
        if (!userForm.getPassword().equals(userForm.getConfirmPassword())) {
            model.addAttribute("passwordError", "Пароли не совпадают");
            return REGISTRATION_VIEW;
        }
        if (!userService.saveUser(userForm)) {
            model.addAttribute("usernameError", "{username.message}");
            return REGISTRATION_VIEW;
        }

        return REDIRECT_LOGIN_VIEW;
    }
}
