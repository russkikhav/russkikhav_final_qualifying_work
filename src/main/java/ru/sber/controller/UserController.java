package ru.sber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.sber.model.human.Role;
import ru.sber.model.human.client.Client;
import ru.sber.service.UserService;

import javax.validation.Valid;

@Controller
@RequestMapping("/users")
@PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_EMPLOYEE')")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {

        this.userService = userService;
    }

    @GetMapping
    public String getUsers(Model model) {
        model.addAttribute("clients", userService.findAll());
        return "user/users";
    }

    @GetMapping("/user_info/{id}")
    public String userInfo(@PathVariable("id") Client client, Model model) {
        model.addAttribute("client", client);
        return "user/user_info";
    }

    @GetMapping("/{id}")
    public String editUserForm(@PathVariable("id") Client client, Model model) {
        model.addAttribute("client", client);
        model.addAttribute("roles", Role.values());
        return "user/userEdit";
    }

    @PostMapping("/{id}")
    public String userEdit(@ModelAttribute("client") @Valid Client client, BindingResult bindingResult, @PathVariable("id") Long id) {
        if (bindingResult.hasErrors()) {
            return "user/userEdit";
        }
        userService.edit(client, id);

        return "redirect:/users";
    }
}
