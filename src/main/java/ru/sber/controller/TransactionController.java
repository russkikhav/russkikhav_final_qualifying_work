package ru.sber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.sber.dto.TransferFormDTO;
import ru.sber.model.bank.card.debit.DebitCard;
import ru.sber.service.DebitCardService;
import ru.sber.service.TransactionService;

import javax.validation.Valid;

@Controller
@RequestMapping("/transfer")
public class TransactionController {
    private static final String TRANSFER_VIEW = "transfer/transfer";
    private static final String REDIRECT_BANK_VIEW = "redirect:/bank";

    private final DebitCardService debitCardService;
    private final TransactionService transactionService;

    @Autowired
    public TransactionController(DebitCardService debitCardService, TransactionService transactionService) {
        this.debitCardService = debitCardService;
        this.transactionService = transactionService;
    }

    @GetMapping("/{id}")
    public String transfer(@PathVariable Long id,
                           @ModelAttribute("transferCard") TransferFormDTO transferCard, Model model) {
        DebitCard debitCard = debitCardService.findById(id);

        model.addAttribute("debit_card_id", debitCard.getId());
        return TRANSFER_VIEW;
    }

    @PostMapping("/{id}")
    public String getTransfer(@PathVariable Long id, Authentication auth,
                              @ModelAttribute("transferCard") @Valid TransferFormDTO transferFormDTO,
                              BindingResult bindingResult, Model model,
                              @RequestParam(required = true, defaultValue = "") String username) {

        if (bindingResult.hasErrors()) {
            return TRANSFER_VIEW;
        }

        transferFormDTO.setName(username);
        transactionService.makeATransfer(debitCardService.findById(id), transferFormDTO);

        return REDIRECT_BANK_VIEW;
    }
}
