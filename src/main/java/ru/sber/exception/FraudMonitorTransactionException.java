package ru.sber.exception;

public class FraudMonitorTransactionException extends RuntimeException {
    public FraudMonitorTransactionException() {
        super();
    }

    public FraudMonitorTransactionException(String message) {
        super(message);
    }
}
