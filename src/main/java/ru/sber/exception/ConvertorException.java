package ru.sber.exception;

public class ConvertorException extends RuntimeException {
    public ConvertorException() {
        super();
    }

    public ConvertorException(String message) {
        super(message);
    }
}
