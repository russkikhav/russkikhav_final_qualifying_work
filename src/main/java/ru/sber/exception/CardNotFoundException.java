package ru.sber.exception;

public class CardNotFoundException extends RuntimeException {
    public CardNotFoundException() {
        super();
    }

    public CardNotFoundException(String message) {
        super(message);
    }
}
